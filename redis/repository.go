package redis

import (
	. "github.com/go-redis/redis"
	"strconv"
	"strings"
	"time"
)

const TTL = 48 * time.Hour

type Repository struct {
	rdb *ClusterClient
}

// Create a new instance of repository
func New(cluster []string, pass string) *Repository {
	var slots []ClusterSlot
	var list []map[string]string
	for _, val := range cluster {
		var res = map[string]string{}
		sub := strings.Split(val, ";")
		for _, subval := range sub {
			parts := strings.Split(subval, "|")
			res[parts[0]] = parts[1]
		}
		list = append(list, res)
	}

	for _, value := range list {
		start, err := strconv.ParseInt(value["Start"], 10, 32)
		if err != nil {
			panic(err)
		}
		end, err := strconv.ParseInt(value["End"], 10, 32)
		if err != nil {
			panic(err)
		}
		slots = append(slots, ClusterSlot{
			Start: int(start),
			End:   int(end),
			Nodes: []ClusterNode{{
				Addr: value["Master"], // 1st master
			}, {
				Addr: value["Slave"], // 1st slave
			}},
		})

	}

	rdb := NewClusterClient(&ClusterOptions{
		ClusterSlots: func() ([]ClusterSlot, error) {
			return slots, nil
		},
		RouteByLatency: true,
		Password:       pass,
		ReadOnly:       true,
	})

	_, err := rdb.Ping().Result()
	if err != nil {
		panic(err)
	}

	err = rdb.ReloadState()
	if err != nil {
		panic(err)
	}

	return &Repository{rdb}
}

// Close a redis connection
func (rep *Repository) Disconnect() {
	if err := rep.rdb.Close(); err != nil {
		panic(err)
	}
}

// Find a entry by id
func (rep *Repository) Find(id string) (string, error) {
	return rep.rdb.Get(id).Result()
}

func (rep *Repository) FindList(id string) ([]string, error) {
	return rep.rdb.LRange(id, 0, -1).Result()
}

// Write a ad
func (rep *Repository) WriteAd(id, value string) (string, error) {
	return rep.rdb.Set(id, value, TTL).Result()
}
