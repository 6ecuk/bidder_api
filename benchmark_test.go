package main

import (
	"github.com/valyala/fasthttp"
	"publisher/integrations"
	"testing"
)

//const ID = "c23b1c03-f468-4101-9bd4-c170acb44a65"
//
//var ctx fasthttp.RequestCtx
//
//func TestMain(m *testing.M) {
//	if err := godotenv.Load("./bin/dev.env"); err != nil {
//		log.Fatalln("No .env file found")
//	}
//	ctx.Request.URI().Update("http://localhost:8090/impress?id=c23b1c03-f468-4101-9bd4-c170acb44a65")
//	ctx.Request.Header.SetMethodBytes([]byte("GET"))
//	ctx.Request.Header.SetContentType("text/html;charset=UTF-8")
//	//ctx.Request.URI().SetQueryStringBytes([]byte(QUERY))
//	ctx.Request.Header.SetUserAgent("test")
//	ctx.Request.Header.SetHost("127.0.0.1")
//	os.Exit(m.Run())
//}

//func BenchmarkTrackerRequestFastHttp(b *testing.B) {
//	req := fasthttp.AcquireRequest()
//	req.URI().Update("http://localhost:8090/impress?id=c23b1c03-f468-4101-9bd4-c170acb44a65")
//	req.Header.SetMethodBytes([]byte("GET"))
//	req.Header.SetContentType("text/html;charset=UTF-8")
//	//ctx.Request.URI().SetQueryStringBytes([]byte(QUERY))
//	res := fasthttp.AcquireResponse()
//	for n := 0; n < b.N; n++ {
//		err := fasthttp.Do(req, res)
//		if err != nil {
//			b.Fatal(err)
//		}
//		if res.StatusCode() != http.StatusMovedPermanently {
//			b.Errorf(fmt.Sprintf("%s response status is %d - error: %s", "Response code different then 200", res.StatusCode(), res.Body()))
//		}
//
//	}
//	fasthttp.ReleaseRequest(req)
//	fasthttp.ReleaseResponse(res)
//}

//func BenchmarkKafka(b *testing.B) {
//	kf := kafka.NewWriter(kafka.WriterConfig{
//		Brokers:       []string{os.Getenv("KAFKA_BROKER")},
//		Topic:         os.Getenv("KAFKA_TOPIC"),
//		Balancer:      &kafka.LeastBytes{},
//		QueueCapacity: 1,
//		BatchSize:     1,
//		Async:         false,
//	})
//	for n := 0; n < b.N; n++ {
//		if ok := kf.WriteMessages(context.Background(), kafka.Message{
//			Key:   []byte("key"),
//			Value: []byte("192.168.10.10"),
//		}); ok != nil {
//			b.Errorf(ok.Error())
//		}
//	}
//}

//func BenchmarkRedis(b *testing.B) {
//	if err := godotenv.Load("./bin/dev.env"); err != nil {
//		log.Fatalln("No .env file found")
//	}
//	rp := redis.New(os.Getenv("REDIS_HOST"), os.Getenv("REDIS_PASS"))
//	for n := 0; n < b.N; n++ {
//		url, err := rp.Find(ID)
//		if err != nil {
//			b.Errorf("Ad not found")
//		}
//		if url == "" {
//			b.Log("Url is empty")
//		}
//	}
//}

//func BenchmarkRouter(b *testing.B) {
//	var dp = api.Dependencies{
//		Redis: redis.New(os.Getenv("REDIS_HOST"), os.Getenv("REDIS_PASS")),
//		Kafka: kafka.NewWriter(kafka.WriterConfig{
//			Brokers:       []string{os.Getenv("KAFKA_BROKER")},
//			Topic:         os.Getenv("KAFKA_TOPIC"),
//			Balancer:      &kafka.LeastBytes{},
//			QueueCapacity: 1,
//			BatchSize:     1,
//			Async:         false,
//			//BatchTimeout:  1 * time.Millisecond,
//		}),
//		Buf: &bytes.Buffer{},
//	}
//	var ap = api.V1{Dependencies: dp}
//	for n := 0; n < b.N; n++ {
//		ap.Router(&ctx)
//	}
//}

func BenchmarkIntegrationCreate(b *testing.B) {
	b.ReportAllocs()
	var ctx fasthttp.RequestCtx
	ctx.Request.Header.SetMethodBytes([]byte("GET"))
	ctx.Request.Header.SetContentType("text/html;charset=UTF-8")
	ctx.Request.URI().SetQueryStringBytes([]byte("api_key=3ce31c573ee349ae90daf48f630a6a55&subscriber_agent=Mozilla%2F5.0%20%28Linux%3B%20Android%208.1.0%3B%20SAMSUNG%20SM-G610F%20Build%2FM1AJQ%29%20AppleWebKit%2F537.36%20%28KHTML%2C%20like%20Gecko%29%20SamsungBrowser%2F8.2%20Chrome%2F63.0.3239.111%20Mobile%20Safari%2F537.36&subscriber_id=0&subscriber_ip=69.236.118.137&subscription_source=0f560e2f-a76c-487a-9176-bdffcee49f6a"))
	ctx.Request.Header.SetUserAgent("test")
	ctx.Request.Header.SetHost("127.0.0.1")
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			integrations.NewIntegrationsList(&ctx)
		}
	})
}
