module publisher

go 1.12

require (
	github.com/Shopify/sarama v1.23.0
	github.com/caarlos0/env/v6 v6.0.0
	github.com/go-redis/redis v6.15.2+incompatible
	github.com/golang/protobuf v1.3.1 // indirect
	github.com/google/uuid v1.1.1
	github.com/joho/godotenv v1.3.0
	github.com/klauspost/compress v1.7.1 // indirect
	github.com/klauspost/cpuid v1.2.1 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/onsi/ginkgo v1.8.0 // indirect
	github.com/onsi/gomega v1.5.0 // indirect
	github.com/segmentio/kafka-go v0.2.5
	github.com/valyala/fasthttp v1.3.0
	github.com/valyala/fastjson v1.4.1
	github.com/valyala/quicktemplate v1.1.1
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4 // indirect
	golang.org/x/net v0.0.0-20190628185345-da137c7871d7 // indirect
	golang.org/x/sys v0.0.0-20190626221950-04f50cda93cb // indirect
	golang.org/x/text v0.3.2 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/jcmturner/goidentity.v3 v3.0.0 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
