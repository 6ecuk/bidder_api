// Code generated by qtc from "bidder_response.qtpl". DO NOT EDIT.
// See https://github.com/valyala/quicktemplate for details.

//line bidder_response.qtpl:1
package templates

//line bidder_response.qtpl:1
import (
	qtio422016 "io"

	qt422016 "github.com/valyala/quicktemplate"
)

//line bidder_response.qtpl:1
var (
	_ = qtio422016.Copy
	_ = qt422016.AcquireByteBuffer
)

//line bidder_response.qtpl:2
type BidderResponse struct {
	Bid      float64
	Response string
}

// JSON marshaling

//line bidder_response.qtpl:9
func (d *BidderResponse) StreamJSON(qw422016 *qt422016.Writer) {
//line bidder_response.qtpl:9
	qw422016.N().S(`[{"Bid":`)
//line bidder_response.qtpl:11
	qw422016.N().F(d.Bid)
//line bidder_response.qtpl:11
	qw422016.N().S(`,"Response":`)
//line bidder_response.qtpl:12
	qw422016.N().Q(d.Response)
//line bidder_response.qtpl:12
	qw422016.N().S(`,}]`)
//line bidder_response.qtpl:14
}

//line bidder_response.qtpl:14
func (d *BidderResponse) WriteJSON(qq422016 qtio422016.Writer) {
//line bidder_response.qtpl:14
	qw422016 := qt422016.AcquireWriter(qq422016)
//line bidder_response.qtpl:14
	d.StreamJSON(qw422016)
//line bidder_response.qtpl:14
	qt422016.ReleaseWriter(qw422016)
//line bidder_response.qtpl:14
}

//line bidder_response.qtpl:14
func (d *BidderResponse) JSON() string {
//line bidder_response.qtpl:14
	qb422016 := qt422016.AcquireByteBuffer()
//line bidder_response.qtpl:14
	d.WriteJSON(qb422016)
//line bidder_response.qtpl:14
	qs422016 := string(qb422016.B)
//line bidder_response.qtpl:14
	qt422016.ReleaseByteBuffer(qb422016)
//line bidder_response.qtpl:14
	return qs422016
//line bidder_response.qtpl:14
}
