package main

import (
	"bytes"
	"flag"
	"github.com/Shopify/sarama"
	"github.com/caarlos0/env/v6"
	"github.com/joho/godotenv"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
	"log"
	"net"
	"os"
	"publisher/api"
	"publisher/redis"
	"time"
)

const ServerName = "api"

type Config struct {
	RedisCluster []string `env:"REDIS_CLUSTER"`
}

var Cfg Config

func init() {
	// loads values from .env into the system
	path := flag.String("config", "./bin/dev.env", "Path to a environment file")
	flag.Parse()
	if err := godotenv.Load(*path); err != nil {
		log.Fatalln("No .env file found")
	}
	if err := env.Parse(&Cfg); err != nil {
		log.Fatalln("Failed to parse env")
	}
}

func main() {
	rp := redis.New(Cfg.RedisCluster, os.Getenv("REDIS_PASS"))
	config := sarama.NewConfig()
	config.Producer.Retry.Max = 10
	config.Producer.Return.Successes = true

	kf, err := sarama.NewSyncProducer(
		[]string{os.Getenv("KAFKA_BROKER_1"), os.Getenv("KAFKA_BROKER_2"), os.Getenv("KAFKA_BROKER_3")},
		config,
	)
	if err != nil {
		panic(err)
	}

	defer func() {
		//TODO rethink this deffer
		rp.Disconnect()
		if err := kf.Close(); err != nil {
			panic(err)
		}
	}()

	dp := api.Dependencies{
		Redis: rp,
		Kafka: kf,
		Json:  &fastjson.ParserPool{},
		Buf:   &bytes.Buffer{},
		Client: &fasthttp.Client{
			MaxConnsPerHost: 10000,
			Dial: func(addr string) (net.Conn, error) {
				return fasthttp.DialTimeout(addr, 30*time.Second)
			},
		},
		BidderHost:    os.Getenv("BIDDER_HOST"),
		KafkaRawTopic: os.Getenv("KAFKA_RAW_TOPIC"),
		KafkaAdTopic:  os.Getenv("KAFKA_AD_TOPIC"),
	}
	ap := api.V1{Dependencies: dp}
	server := fasthttp.Server{
		Handler: ap.Router,
		Name:    ServerName,
	}
	if err := server.ListenAndServe(":8090"); err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
