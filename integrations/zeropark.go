package integrations

import (
	"github.com/valyala/fasthttp"
	"net/url"
	. "publisher/templates"
)

const ZeroparkUrl = "http://push.zeropark.com/br/"

type Zeropark struct {
	token     string
	userId    string
	userIp    string
	userAgent string
	secure    string
	domainId  string
	language  string
	format    string
	pubId     string
}

func (z *Zeropark) NewAd() BidAd {
	return BidAd{
		Platform:       "ZeroPark",
		Url:            z.url(),
		RequestType:    fasthttp.MethodGet,
		ContentType:    "",
		Body:           "",
		ResponseFormat: "JsonSingle",
		Map: map[string]string{
			"icon_url":    "imp_url",
			"url":         "link",
			"description": "description",
		},
	}
}

func (z *Zeropark) url() string {
	return ZeroparkUrl + z.token + "?ua=" + url.QueryEscape(z.userAgent) + "&secure=" + z.secure + "&ip=" + z.userIp + "&user_id=" +
		z.userId + "&domain=" + z.domainId + "&lang=" + z.language + "&format=" + z.format + "&pubid=" + z.pubId
}
