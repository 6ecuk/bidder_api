package integrations

import (
	"github.com/valyala/fasthttp"
	"net/url"
	. "publisher/templates"
)

const DatsPushUrl = "https://dsp-eu.datspush.com/v1/external/feed/"

type DatsPush struct {
	token        string
	userIp       string
	userAgent    string
	sourceId     string
	subscriberId string
	language     string
	createdAt    string
}

func (d *DatsPush) NewAd() BidAd {
	return BidAd{
		Platform:       "DatsPush",
		Url:            d.url(),
		RequestType:    fasthttp.MethodGet,
		ContentType:    "",
		Body:           "",
		ResponseFormat: "JsonArray",
		Map: map[string]string{
			"icon_url":    "icon_url",
			"url":         "click_url",
			"description": "text",
		},
	}
}

func (d *DatsPush) url() string {
	return DatsPushUrl + d.token + "?user_agent=" + url.QueryEscape(d.userAgent) + "&user_ip=" + d.userIp + "&source_id=" + d.sourceId +
		"&lang=" + d.language + "&subscriber_id=" + d.subscriberId + "&subscriber_date=" + d.createdAt
}
