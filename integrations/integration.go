package integrations

import (
	"github.com/valyala/fasthttp"
	. "publisher/templates"
	"time"
)

const (
	TIMEFORMAT         = "2006-01-02"
	SubscriberIp       = "subscriber_ip"
	SubscriberAgent    = "subscriber_agent"
	SubscriberLanguage = "subscriber_language"
	SubscriberId       = "subscriber_id"
	SubscriberSource   = "subscription_source"
	IntegrationList    = "integrations"
	PubId              = "pub_id"
)

type Ad interface {
	NewAd() BidAd
}

// Create a BidAds structures from the request context
func NewIntegrationsList(ctx *fasthttp.RequestCtx) (*AdList, error) {
	//list, ok := ctx.UserValue(IntegrationList).([]string)
	//if !ok {
	//	return nil, errors.New("the integrations list is invalid")
	//}
	//pubid, ok := ctx.UserValue(PubId).(string)
	//if !ok {
	//	return nil, errors.New("the pub id is not a string type")
	//}
	//if len(list) == 0 {
	//	return nil, errors.New("the integrations list is empty")
	//}
	var bidList []BidAd
	//for _, integration := range list {
	//	switch integration {
	//	case "Zeropark":
	//		ad := &Zeropark{
	//			token:     "e6113ce0-9beb-11e9-81a3-0a15cb739170",
	//			userId:    string(ctx.QueryArgs().Peek(SubscriberId)),
	//			userIp:    string(ctx.QueryArgs().Peek(SubscriberIp)),
	//			userAgent: string(ctx.QueryArgs().Peek(SubscriberAgent)),
	//			secure:    "true",
	//			domainId:  string(ctx.QueryArgs().Peek(SubscriberSource)),
	//			language:  string(ctx.QueryArgs().Peek(SubscriberLanguage)),
	//			format:    "json",
	//			pubId:     pubid,
	//		}
	//		bidList = append(bidList, ad.NewAd())
	//	case "DatsPush":
	//		ad := &DatsPush{
	//			token:     "hERtfc8tSzmE",
	//			userAgent: string(ctx.QueryArgs().Peek(SubscriberAgent)),
	//			sourceId:  string(ctx.QueryArgs().Peek(SubscriberSource)),
	//			userIp:    string(ctx.QueryArgs().Peek(SubscriberIp)),
	//			createdAt: time.Now().Local().Format(TIMEFORMAT),
	//			language:  string(ctx.QueryArgs().Peek(SubscriberLanguage)),
	//		}
	//		bidList = append(bidList, ad.NewAd())
	//	default:
	//		return nil, errors.New(fmt.Sprintf("the integration: %s doesn't exist", integration))
	//	}
	//}

	//ad := &Zeropark{
	//	token:     "e6113ce0-9beb-11e9-81a3-0a15cb739170",
	//	userId:    string(ctx.QueryArgs().Peek(SubscriberId)),
	//	userIp:    string(ctx.QueryArgs().Peek(SubscriberIp)),
	//	userAgent: string(ctx.QueryArgs().Peek(SubscriberAgent)),
	//	secure:    "true",
	//	domainId:  string(ctx.QueryArgs().Peek(SubscriberSource)),
	//	language:  string(ctx.QueryArgs().Peek(SubscriberLanguage)),
	//	format:    "json",
	//	pubId:     pubid,
	//}
	//bidList = append(bidList, ad.NewAd())

	ads := &DatsPush{
		token:     "hERtfc8tSzmE",
		userAgent: string(ctx.QueryArgs().Peek(SubscriberAgent)),
		sourceId:  string(ctx.QueryArgs().Peek(SubscriberSource)),
		userIp:    string(ctx.QueryArgs().Peek(SubscriberIp)),
		createdAt: time.Now().Local().Format(TIMEFORMAT),
		language:  string(ctx.QueryArgs().Peek(SubscriberLanguage)),
	}
	bidList = append(bidList, ads.NewAd())

	return &AdList{List: bidList}, nil
}
