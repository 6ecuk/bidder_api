package api

import (
	"bytes"
	"fmt"
	"github.com/Shopify/sarama"
	"github.com/google/uuid"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
	"log"
	intr "publisher/integrations"
	"publisher/redis"
	"publisher/templates"
	"strconv"
	"time"
)

const (
	Key               = "event"
	DefaultPubPercent = 80 // A publisher percents
	TIMEFORMAT        = "2006-01-02 15:04:05"
	PushWebsiteId     = "push_website_id"
	PushSubscriberId  = "push_subscriber_id"
	PushData          = "push_data"
	Token             = "api_key"

	TrackingImpression = "https://tracking.revquake.com/impress?id="
	TrackingClick      = "https://tracking.revquake.com/click?id="
	DecimalScale       = 10000
)

var (
	advertList = map[string]string{
		"ZeroPark": "7e6ab572-ec80-48a2-9de4-204cb20f910d",
		"DatsPush": "fe914012-00d1-4b6b-a809-36f546d606ad",
	}
	parametersList = [5]string{Token, intr.SubscriberIp, intr.SubscriberAgent, intr.SubscriberId, intr.SubscriberSource}
)

type V1 struct {
	Dependencies
}

type Dependencies struct {
	Redis         *redis.Repository
	Kafka         sarama.SyncProducer
	Json          *fastjson.ParserPool
	Buf           *bytes.Buffer
	Client        *fasthttp.Client
	BidderHost    string
	KafkaAdTopic  string
	KafkaRawTopic string
}

type Finder interface {
	Find(id string) (string, error)
	FindList(id string) ([]string, error)
}

type AdWriter interface {
	WriteAd(id, value string) (string, error)
}

// Router for a v1 api version
func (a *V1) Router(ctx *fasthttp.RequestCtx) {
	if ctx.IsGet() {
		switch string(ctx.Path()) {
		case "/v1/feed":
			if a.isValid(ctx) {
				a.feedHandler(ctx, a.Redis)
			}
			return
		}
	}
	ctx.Error("Page not found", fasthttp.StatusNotFound)
}

func (a *V1) isValid(ctx *fasthttp.RequestCtx) bool {
	for _, pn := range parametersList {
		pv := ctx.QueryArgs().Peek(pn)
		if len(pv) == 0 {
			ctx.Error(fmt.Sprintf("Request missed parameter %s", pn), fasthttp.StatusBadRequest)
			return false
		}
	}
	return true
}

func (a *V1) feedHandler(ctx *fasthttp.RequestCtx, rp Finder) {
	p := fastjson.Parser{}
	buf := bytes.Buffer{}
	defer buf.Reset()
	buf.Write(ctx.QueryArgs().Peek(Token))
	token := buf.String()
	buf.Reset()
	json, err := rp.Find(token)
	if err != nil || json == "" {
		ctx.Error("Publisher not found", fasthttp.StatusNotFound)
		return
	}
	v, err := p.Parse(json)
	if err != nil {
		ctx.Error(err.Error(), fasthttp.StatusInternalServerError)
		return
	}
	pubId := string(v.GetStringBytes("id"))
	//externalList, err := rp.FindList(fmt.Sprintf("integrations:%s", pubId))
	//if err != nil || len(externalList) == 0 {
	//	log.Println(fmt.Sprintf("The external list of publisher: %s is missed", pubId))
	//	ctx.Error("", fasthttp.StatusInternalServerError)
	//	return
	//}
	//ctx.SetUserValue(intr.IntegrationList, externalList)
	ctx.SetUserValue(intr.PubId, pubId)
	integrationList, err := intr.NewIntegrationsList(ctx)
	if err != nil {
		log.Println(err.Error())
		ctx.Error("", fasthttp.StatusInternalServerError)
		return
	}
	integrationList.WriteJSON(&buf)

	req := fasthttp.AcquireRequest()
	req.URI().Update(a.Dependencies.BidderHost)
	req.Header.SetContentType("application/json")
	req.Header.SetMethod(fasthttp.MethodPost)
	req.SetBody(buf.Bytes())
	res := fasthttp.AcquireResponse()
	err = a.Client.Do(req, res)
	buf.Reset()
	if err != nil {
		log.Println(err.Error())
		ctx.Error("", fasthttp.StatusInternalServerError)
		return
	}
	if res.Header.StatusCode() == fasthttp.StatusNoContent {
		ctx.Error("Content not found", fasthttp.StatusNoContent)
		return
	}
	v, err = p.ParseBytes(res.Body())
	if err != nil {
		log.Println(err.Error())
		ctx.Error("", fasthttp.StatusInternalServerError)
		return
	}
	pl := v.GetStringBytes("response")
	var plz = make([]byte, len(pl))
	copy(plz, pl)
	adv := v.GetStringBytes("platform")
	bid := v.GetFloat64("bid")
	if isZeroFloat(bid) {
		ctx.Error("Content not found", fasthttp.StatusNoContent)
		return
	}

	payOut := a.payOutCalculation(token, bid)

	dscMap := string(v.GetStringBytes("description"))
	urlMap := string(v.GetStringBytes("url"))
	ocnMap := string(v.GetStringBytes("icon_url"))
	id := uuid.New().String()
	crt := time.Now().Local().Format(TIMEFORMAT)
	buf.Write(adv)
	advz := buf.String()
	buf.Reset()

	rpv, err := p.ParseBytes(pl)
	if err != nil {
		log.Println(err.Error())
		log.Println(pl)
		ctx.Error("", fasthttp.StatusInternalServerError)
		return
	}
	strz := templates.KafkaRaw{
		Id:           id,
		AdvertiserId: advertList[advz],
		PublisherId:  pubId,
		Request:      ctx.Request.URI().FullURI(),
		Payload:      plz,
		Response:     []byte(""),
		Created_at:   crt,
	}
	strz.WriteJSON(&buf)
	if partition, offset, ok := a.Kafka.SendMessage(&sarama.ProducerMessage{
		Topic: a.Dependencies.KafkaRawTopic,
		Value: sarama.ByteEncoder(buf.Bytes()),
	}); ok != nil {
		log.Println(fmt.Sprintf("Kafka writer %s by partition: %d offeset: %d", ok.Error(), partition, offset))
		ctx.Error("", fasthttp.StatusNoContent)
		buf.Reset()
		return
	}

	buf.Reset()
	buf.Write(ctx.QueryArgs().Peek(PushWebsiteId))
	wid := buf.String()
	buf.Reset()
	buf.Write(ctx.QueryArgs().Peek(PushSubscriberId))
	sbz := buf.String()
	buf.Reset()
	buf.Write(ctx.QueryArgs().Peek(PushData))
	dtz := buf.String()
	buf.Reset()
	inp := strconv.FormatFloat(payOut, 'f', 4, 64)
	adId := uuid.New().String()
	title := string(rpv.GetStringBytes("title"))
	body := string(rpv.GetStringBytes(dscMap))
	ads := templates.KafkaAd{
		Id:                 adId,
		RawId:              id,
		ManagerId:          "",
		WebsiteId:          wid,
		AdvertiserId:       advertList[advz],
		PublisherId:        pubId,
		SubscriberId:       sbz,
		Actions:            "",
		Badge:              "",
		Body:               body,
		Data:               dtz,
		Dir:                "",
		Lang:               "",
		Tag:                "",
		Icon:               string(rpv.GetStringBytes(ocnMap)),
		Image:              "",
		Renotify:           "",
		RequireInteraction: "",
		Silent:             "",
		Timestamp:          "",
		Title:              title,
		Vibrate:            "",
		Type:               "cpc",
		ExternalPrice:      strconv.FormatFloat(bid, 'f', 4, 64),
		InternalPrice:      inp,
		Url:                string(rpv.GetStringBytes(urlMap)),
		Created_at:         crt,
	}
	ads.WriteJSON(&buf)
	if partition, offset, ok := a.Kafka.SendMessage(&sarama.ProducerMessage{
		Topic: a.Dependencies.KafkaAdTopic,
		Value: sarama.ByteEncoder(buf.Bytes()),
	}); ok != nil {
		log.Println(fmt.Sprintf("Kafka writer %s by partition: %d offeset: %d", ok.Error(), partition, offset))
		ctx.Error("", fasthttp.StatusNoContent)
		buf.Reset()
		return
	}
	buf.Reset()
	if _, ok := a.Redis.WriteAd(adId, ads.JSON()); ok != nil {
		log.Println(ok.Error())
		ctx.Error("", fasthttp.StatusNoContent)
		return
	}
	//TODO reduce memory allocation in string concatenations
	tst := templates.Response{
		Id:          adId,
		Title:       title,
		Description: body,
		Cpc:         inp,
		IconUrl:     TrackingImpression + adId,
		ImageUrl:    TrackingImpression + adId,
		ClickUrl:    TrackingClick + adId,
	}
	list := make([]templates.Response, 0)
	list = append(list, tst)
	rsp := templates.ResponseList{
		List: list,
	}
	rsp.WriteJSON(&buf)
	ctx.SetContentType("application/json")
	ctx.SetBody(buf.Bytes())
}

func (a *V1) payOutCalculation(token string, bid float64) float64 {
	var payOut float64
	parser := a.Json.Get()
	defer a.Json.Put(parser)
	result, err := a.Redis.Find(token)
	if result == "" || err != nil {
		return defaultPayout(bid)
	}
	v, err := parser.Parse(result)
	if err != nil {
		log.Println(err)
		return defaultPayout(bid)
	}
	payOutPercent := v.GetFloat64("payout")
	if payOutPercent == 0 {
		return defaultPayout(bid)
	}
	payOut = (bid * payOutPercent) / 100
	if isZeroFloat(payOut) {
		return 0.0
	}
	return payOut
}

func defaultPayout(bid float64) float64 {
	payOut := bid * DefaultPubPercent / 100
	if isZeroFloat(payOut) {
		return 0.0
	}
	return payOut
}

func isZeroFloat(fl float64) bool {
	if int(fl*DecimalScale)%DecimalScale == 0 {
		return true
	}
	return false
}
